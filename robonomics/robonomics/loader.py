from base64 import encode
import sys
import robonomicsinterface as RI
import ipfshttpclient as IC
import logging as log

log.basicConfig(filename='robonomics_loader.log', level=log.DEBUG)
log.info("start robonomics_loader")

directory = sys.argv[1]
seed = sys.argv[2]

ipfs_client = IC.connect()
ipfs_add_list = ipfs_client.add(directory, pattern='*.pddl')

hash = ipfs_add_list[len(ipfs_add_list)-1].get('Hash')
log.info("directory (" + directory + ") hash:" + hash)

ri_interface = RI.RobonomicsInterface(seed=seed, remote_ws='ws://127.0.0.1:9944')
H256_hash = ri_interface.ipfs_qm_hash_to_32_bytes(hash)
addr = ri_interface.define_address()

launch_res = ri_interface.send_launch(target_address=addr, parameter=H256_hash)

log.info("robonomics_interface launch result:" + launch_res)
