# Robonomics ROS2 package

> **On alpha developement stage! Don't use in production!**

Package for interoperability between Robonomics Blockchain and ROS2 Planning System.

## Workflow
- Subscribe for events in local robonomics node
- When event recieved convert `H256` hash to `IPFS` hash and download it's content from IPFS network
- Validate `domain` and `problem` PDDL specifications through `GetPlan` Plansys2 service. If Succeed add problem to Plansys2 and get generated plan
- Execute plan with Plansys2 `Executor` 

## Supported robonomics events
- `NewLaunch`

## Requrements
- `go-ipfs` 0.7.0
- `robonomics` blockchain node 
- Python packages
    - `robonomicsinterface` 0.10.1
    - `ipfshttpclient` 0.7.0
- ROS2 packages
    - `plansys2`

## Future plans
- `Write Datalog` on successful plan execution
- `Write Datalog` on every completed action in plan
- Support other Robonomics Extrinsics:
    - `Digital Twin`
    - `Liability`
    - `RWS`