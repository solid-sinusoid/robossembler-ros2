-- env_manager configuraiton file
include "nodes.lua"
include "utils/utils.lua"

-- include environments configs
include "environments/simulator.lua"
include "environments/ground_true.lua"

-- env_manager configuration
ENV_MANAGER = {
    environments = { 
        SIMULATOR, GROUND_TRUE 
    },
    nodes = NODES 
}

check_nodes(ENV_MANAGER)

return ENV_MANAGER
