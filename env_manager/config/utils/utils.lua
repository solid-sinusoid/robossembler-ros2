function check_nodes(config)
    for env, cfg in pairs(config.environments) do 
        for comp, opt in pairs(cfg.components) do 
            assert(config.nodes[comp] ~= nil, "not all nodes presented.")
            assert(opt.lib ~= nil, "not library provided.")
            assert(opt.class ~= nil, "not class provided.")
        end
    end
end
