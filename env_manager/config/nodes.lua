NODES = {
    talker_node = {
        name = "talker",
        type = "Publisher",
        msg_type = "std_msgs/String",
    },
    service_node = {
        name = "add_two_ints",
        type = "Service",
        msg_type = "example_interfaces/AddTwoInts"
    }
}
