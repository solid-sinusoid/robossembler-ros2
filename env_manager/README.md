## Environment Manager
Environment Manager is a package that allow you to create custom environments based on *component*, switch between different environments, synchronization of different environments with each other.

![](docs/assets/env_manager.png)

### Getting Started
To get a local copy up and running follow these simple example steps.

#### Prerequisites
```bash
# install glog
apt-get install libgoogle-glog-dev

#install lua
wget http://www.lua.org/ftp/lua-5.3.1.tar.gz
tar zxf lua-5.3.1.tar.gz
make -C lua-5.3.1 linux && make -C lua-5.3.1 install
```
### Config
Configuration is done by lua.
```lua

-- require configuration table
ENV_MANAGER = {
    -- dictionary of environments
    environments = {
        -- environment to upload with manager
        example_environment = {
            -- namespace in which all components will be
            namespace = "<namespace_name>",
            -- dictionary of components needed to upload for current environment
            components = { 
                -- node match one of node in ENV_MANAGER.nodes
                example_node = {
                    -- example libpub_component.so
                    lib = "<library_name>"
                    -- name of class which implement example_node logic
                    class = "<class_name>" 
                }
            },
        }, 
    },
    -- dictionary of nodes, that will be created by env_manager
    nodes = {
        -- node for initialization by env_manger
        example_node = {
            -- node name
            name = "<node_name>",
            -- one node can produce only one topic/service
            type = "<Publisher|Subscriber|Service|Client>",
            -- example "std_msgs/String",
            msg_type = "<msg_type>",
        },
    }, 
}

-- return configuration table
return ENV_MANAGER
```
You can safely use all the feature of `lua` to write configuration.
### Usage
Create component library.
```c++
#include "component_manager/publisher_component.hpp"
#include <chrono>
#include <iostream>
#include <memory>
#include <utility>

#include "rclcpp/rclcpp.hpp"
#include <std_msgs/msg/string.hpp>

namespace pub_component
{
using namespace std::chrono_literals;

class Publisher: public env_manager::component_manager::PublisherComponent<std_msgs::msg::String>
{
public:
    explicit Publisher(const rclcpp::NodeOptions &options)
        : env_manager::component_manager::PublisherComponent<std_msgs::msg::String>(options)
    {
        timer_ = create_wall_timer(1s, std::bind(&Publisher::on_timer, this));
    }

protected:
    void on_timer()
    {
        auto msg = std::make_unique<std_msgs::msg::String>();
        msg->data = "Hello World!";
        RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", msg->data.c_str());
        std::flush(std::cout);

        this->populate_publication(*msg.get());
    }


private:
    rclcpp::TimerBase::SharedPtr timer_;
};
} // namespace pub_component
#include "rclcpp_components/register_node_macro.hpp"

RCLCPP_COMPONENTS_REGISTER_NODE(pub_component::Publisher)
```
```cmake
add_library(pub_component SHARED
  src/pub_component.cpp)
target_compile_definitions(pub_component
  PRIVATE "COMPOSITION_BUILDING_DLL")
ament_target_dependencies(pub_component
  "rclcpp"
  "env_manager"
  "rclcpp_components"
  "std_msgs")
rclcpp_components_register_nodes(pub_component "pub_component::Publisher")
set(node_plugins "${node_plugins}pub_component::Publisher;$<TARGET_FILE:pub_component>\n")
```
Add component_library to your configuration file.
```lua
ENV_MANAGER = {
    environments = {
        example_environment = {
            namespace = "<namespace_name>",
            components = { 
                example_node = {
                    lib = "libpub_component.so"
                    class = "pub_component::Publisher" 
                }
            },
        }, 
    },
    nodes = {
        example_node = {
            name = "<node_name>",
            type = "Publisher",
            msg_type = "std_msgs/String",
        },
    }, 
}
return ENV_MANAGER
```
Start env_manager
```bash
cd ${WS_DIR} && .install/setup.bash
./install/env_manager/lib/env_manager/main
```
### License
Distributed under the Apache 2.0 License. See LICENSE for more information.
