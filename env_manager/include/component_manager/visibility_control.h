#ifndef COMPONENT_MANAGER__VISIBILITY_CONTROL_H_
#define COMPONENT_MANAGER__VISIBILITY_CONTROL_H_

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define COMPONENT_MANAGER_EXPORT __attribute__ ((dllexport))
    #define COMPONENT_MANAGER_IMPORT __attribute__ ((dllimport))
  #else
    #define COMPONENT_MANAGER_EXPORT __declspec(dllexport)
    #define COMPONENT_MANAGER_IMPORT __declspec(dllimport)
  #endif
  #ifdef COMPONENT_MANAGER_BUILDING_LIBRARY
    #define COMPONENT_MANAGER_PUBLIC COMPONENT_MANAGER_EXPORT
  #else
    #define COMPONENT_MANAGER_PUBLIC COMPONENT_MANAGER_IMPORT
  #endif
  #define COMPONENT_MANAGER_PUBLIC_TYPE COMPONENT_MANAGER_PUBLIC
  #define COMPONENT_MANAGER_LOCAL
#else
  #define COMPONENT_MANAGER_EXPORT __attribute__ ((visibility("default")))
  #define COMPONENT_MANAGER_IMPORT
  #if __GNUC__ >= 4
    #define COMPONENT_MANAGER_PUBLIC __attribute__ ((visibility("default")))
    #define COMPONENT_MANAGER_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define COMPONENT_MANAGER_PUBLIC
    #define COMPONENT_MANAGER_LOCAL
  #endif
  #define COMPONENT_MANAGER_PUBLIC_TYPE
#endif

#endif  // COMPONENT_MANAGER__VISIBILITY_CONTROL_H_
