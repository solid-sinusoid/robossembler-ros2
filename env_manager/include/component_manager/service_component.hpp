#ifndef ENV_MANAGER__COMPONENT_MANAGER__SERVICE_COMPONENT_HPP_
#define ENV_MANAGER__COMPONENT_MANAGER__SERVICE_COMPONENT_HPP_

#include "component_manager/visibility_control.h"

#include <rclcpp/node.hpp>

namespace env_manager
{
namespace component_manager
{

const std::string DEFAULT_SERVICE_NDOE_NAME = "env_manager_service_node";
const std::string DEFAULT_SERVICE_NAME = "service";

template <typename ServiceT>
class ServiceComponent: public rclcpp::Node
{
public:
    explicit ServiceComponent(const rclcpp::NodeOptions& options)
        : Node(DEFAULT_SERVICE_NDOE_NAME, options)
    {
        _service = create_service<ServiceT>(
            DEFAULT_SERVICE_NAME, std::bind(
                &ServiceComponent::callback, this, 
                std::placeholders::_1, std::placeholders::_2));
    }

    virtual void callback(std::shared_ptr<typename ServiceT::Request> request,
        std::shared_ptr<typename ServiceT::Response> response) = 0;

private:
    typename rclcpp::Service<ServiceT>::SharedPtr _service;
};

} // namespace component_manager
} // namespace env_manager

#endif // ENV_MANAGER__COMPONENT_MANAGER__SERVICE_COMPONENT_HPP_
