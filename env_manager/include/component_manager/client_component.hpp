#ifndef ENV_MANAGER__COMPONENT_MANAGER__CLIENT_COMPONENT_HPP_
#define ENV_MANAGER__COMPONENT_MANAGER__CLIENT_COMPONENT_HPP_

#include "component_manager/visibility_control.h"

#include "rclcpp/rclcpp.hpp"

#include <chrono>
#include <cstdlib>
#include <future>
#include <memory>


namespace env_manager
{
namespace component_manager
{

using namespace std::chrono_literals;

const std::string DEFAULT_CLIENT_NODE_NAME = "env_manager_client_node";
const std::string DEFAULT_CLIENT_NAME = "client";

template <typename ServiceT>
class ClientComponent: public rclcpp::Node
{
public:
    explicit ClientComponent(const rclcpp::NodeOptions& options)
        : Node(DEFAULT_CLIENT_NODE_NAME, options)
    {
        _client = create_client<ServiceT>(DEFAULT_CLIENT_NAME, 10);
    }

    virtual void callback(
            typename rclcpp::Client<ServiceT>::SharedFuture future) = 0;

    void populate_request(
            const std::shared_ptr<typename ServiceT::Request>& request)
    {
        while (!_client->wait_for_service(1s)) 
        {
            if (rclcpp::ok()) 
            {
                RCLCPP_ERROR(this->get_logger(),
                "Client interrupted while waiting for service. Terminating...");
            }
        }

        auto result_future = _client->async_send_request(
            request, std::bind(&ClientComponent::callback, 
                               this, std::placeholders::_1));
    }

private:
    typename rclcpp::Client<ServiceT>::SharedPtr _client;
};

} // namespace component_manager
} // namespace env_manager

#endif // ENV_MANAGER__COMPONENT_MANAGER__CLIENT_COMPONENT_HPP_
