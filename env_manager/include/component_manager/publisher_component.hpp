#ifndef ENV_MANAGER__COMPONENT_MANAGER__PUBLISHER_COMPONENT_HPP_
#define ENV_MANAGER__COMPONENT_MANAGER__PUBLISHER_COMPONENT_HPP_

#include "component_manager/visibility_control.h"

#include "rclcpp/rclcpp.hpp"

namespace env_manager
{
namespace component_manager
{

const std::string DEFAULT_PUB_NODE_NAME = "env_manager_pub_node";
const std::string DEFAULT_PUB_TOPIC_NAME = "pub_topic";

template <typename MessageT>
class PublisherComponent: public rclcpp::Node
{
public:
    explicit PublisherComponent(const rclcpp::NodeOptions& options)
        : Node(DEFAULT_PUB_NODE_NAME, options)
    {
        _pub = create_publisher<MessageT>(DEFAULT_PUB_TOPIC_NAME, 10);
        auto ret = rcutils_logging_set_logger_level(
            get_logger().get_name(), RCUTILS_LOG_SEVERITY_FATAL);
        if (ret != RCUTILS_RET_OK) 
        {
            RCLCPP_ERROR(get_logger(), 
                "Error setting severity: %s", rcutils_get_error_string().str);
            rcutils_reset_error();
        }
    }

    void populate_publication(const MessageT& msg)
    {
        _pub->publish(std::move(msg));
    }

private:
    typename rclcpp::Publisher<MessageT>::SharedPtr _pub;
};

} // namespace component_manager
} // namespace env_manager

#endif // ENV_MANAGER__COMPONENT_MANAGER__PUBLISHER_COMPONENT_HPP_
