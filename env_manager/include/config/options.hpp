#ifndef  ENV_MANAGER_CONFIG_OPTIONS_HPP_
#define ENV_MANAGER_CONFIG_OPTIONS_HPP_

#include "config/lua_file_resolver.hpp"

#include <unordered_map>

namespace env_manager
{
namespace config
{

struct ComponentOption
{
    static ComponentOption create_option(
        LuaParameterDictionary* lua_parameter_dictionary);
    std::string library;
    std::string class_name;
};

struct EnvironmentOption
{
    static EnvironmentOption create_option(
        LuaParameterDictionary* lua_parameter_dictionary);
    std::string ns;
    std::map<std::string, ComponentOption> components;
};

struct NodeOption 
{
    enum NodeType
    {
        Publisher,
        Subscriber,
        Service,
        Client
    };

    static NodeOption create_option(
        LuaParameterDictionary* lua_parameter_dictionary);
    std::string name;
    std::string msg_type;
    NodeType type;
    static const std::unordered_map<std::string, NodeType> node_type_remap;
};

struct EnvManagerOption 
{
    static EnvManagerOption create_option(
        LuaParameterDictionary* lua_parameter_dictionary); 
    std::map<std::string, EnvironmentOption> environments;
    std::map<std::string, NodeOption> nodes;
};

}
}
#endif // ENV_MANAGER_CONFIG_OPTIONS_HPP_
