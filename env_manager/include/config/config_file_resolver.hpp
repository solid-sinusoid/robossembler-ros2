#ifndef ENV_MANAGER_CONFIG_CONFIG_FILE_RESOLVER_H_
#define ENV_MANAGER_CONFIG_CONFIG_FILE_RESOLVER_H_

#include "lua_file_resolver.hpp"

namespace env_manager
{
namespace config
{

class ConfigurationFileResolver : public FileResolver
{
public:
    explicit ConfigurationFileResolver(
        const std::vector<std::string>& cfg_files_dirs);

    std::string GetPath(const std::string& basename) const;
    std::string GetContent(const std::string& basename) const;

private:
    std::vector<std::string> _config_files_dirs;

};

} // namespace config
} // namespace env_manager

#endif // ENV_MANAGER_CONFIG_CONFIG_FILE_RESOLVER_H_
