#include "component_manager/component_manager.hpp"
#include "config/config_file_resolver.hpp"
#include "config/lua_file_resolver.hpp"
#include "config/options.hpp"

#include "config/config.hpp"

#include "glog/logging.h"

int main(int argc, const char* argv[])
{
    google::InitGoogleLogging(argv[0]);
    FLAGS_logtostderr = 1;
    
    rclcpp::init(argc, argv);
    auto exec = std::make_shared<rclcpp::executors::SingleThreadedExecutor>();

    std::vector<std::string> dirs = {};
    auto cfg_resolver = 
        ::std::make_unique<::env_manager::config::ConfigurationFileResolver>(
            std::vector<std::string>{
                std::string(env_manager::config::kSourceDirectory) +
                "/config"
            }
        );

    auto code = cfg_resolver->GetContent("config.lua");
    //LOG(INFO) << code;
    ::env_manager::config::LuaParameterDictionary lua_dict(
            code, std::move(cfg_resolver)
        );

    auto env_manager_opts = 
        ::env_manager::config::EnvManagerOption::create_option(
            &lua_dict
        );

    std::vector<env_manager::component_manager::ComponentManager> comp_mngrs;

    for (const auto& [env, opts]: env_manager_opts.environments)
    {
        LOG(INFO) << "Start to initialize environment: " << env;
        env_manager::component_manager::ComponentManager cmg(exec);
        cmg.register_components(opts.components, env_manager_opts.nodes, opts.ns);
        //cmg.upload_components_to_executor(&exec);
        comp_mngrs.push_back(std::move(cmg));
    }

    //std::string ns = "/";
    //comp_mngrs.begin()->remap_components_namespace(ns);

    exec->spin();

    for (auto &cmg: comp_mngrs)
        cmg.remove_components_from_executor();

    rclcpp::shutdown();

    return 0;
}
