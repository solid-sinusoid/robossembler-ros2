#include "config/config_file_resolver.hpp"

#include <fstream>
#include <iostream>
#include <streambuf>

#include "config/config.hpp"

#include <glog/logging.h>

namespace env_manager
{
namespace config
{

ConfigurationFileResolver::ConfigurationFileResolver(
    const std::vector<std::string>& cfg_files_dirs)
    : _config_files_dirs(cfg_files_dirs)
{
    _config_files_dirs.push_back(kConfigurationFilesDirectory);
}

std::string ConfigurationFileResolver::GetPath(
    const std::string &basename) const
{
    for (const auto& path: _config_files_dirs)
    {
        const std::string filename = path + "/" + basename;
        std::ifstream stream(filename.c_str());
        if (stream.good())
        {
            LOG(INFO) << "found filename: " << filename;
            return filename;
        }   
    }

    LOG(FATAL) << "File '" << basename << "' was not found.";
}

std::string ConfigurationFileResolver::GetContent(
    const std::string &basename) const
{
    CHECK(!basename.empty()) << "File basename cannot be empty." << basename;
    const std::string filename = GetPath(basename);
    std::ifstream stream(filename.c_str());
    return std::string(std::istreambuf_iterator<char>(stream),
                       std::istreambuf_iterator<char>());
}

} // namespace config
} // namespace env_manager
