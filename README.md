# Robossembler ROS2

Repo for ROS2 packages related to Robossembler

## Instructions
### Requirements
* OS: Ubuntu 22.04
* ROS 2 Humble

### Dependencies
These are the primary dependencies required to use this project.

* MoveIt 2 
    > Install/build a version based on the selected ROS 2 release
* Gazebo Fortress

### Build

1. Clone the repository
2. Build packages `colcon build`


Prepare workspace & install dependencies (So far only tested with UR robot arm)
```
mkdir -p ~/robossembler_ws/src && cd ~/robossembler_ws/src
git clone https://gitlab.com/robosphere/robossembler-ros2
vcs import . < robossembler-ros2/rbs.repos
cd ..
rosdep install -y -r -q --from-paths src --ignore-src --rosdistro humble
colcon build
```

### Examples
Add source to environment
```
. install/setup.bash
```

Launch MoveIt2, Gazebo, RViz
```bash
ros2 launch rbs_simulation rbs_simulation.launch.py
```
#### Launch bt_tree

It will execute `bt_tree` once from file `rbs_bt_executor/bt_trees/test_tree.xml`

```
ros2 launch rbs_executor rbs_executor.launch.py
```

The robot arm should move to the point from config file in path 
```
rbs_bt_executor/config/points.yaml
```

### Links
* [bt_v3_cpp](https://www.behaviortree.dev)
* [moveit2](https://moveit.picknik.ai/humble/index.html)